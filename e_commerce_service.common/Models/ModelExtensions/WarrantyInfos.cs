﻿using e_commerce_service.common.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace e_commerce_service.common.Models.ModelExtensions {
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy), ItemNullValueHandling = NullValueHandling.Ignore)]
    public class WarrantyInfos {
        /// <summary>
        /// Cờ xác nhận sản phẩm có gói bảo hành hay ko
        /// </summary>
        public bool HasWarranty { get; set; } = false;

        /// <summary>
        /// Thông tin bảo hành cho sản phẩm
        /// </summary>
        public string? WarrantyId { get; set; } = string.Empty;

        public string? WarrantyPolicy { get; set; }

        public List<WarrantyForm>? WarrantyForms { get; set; }
    }
}
