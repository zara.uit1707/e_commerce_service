﻿using e_commerce_service.common.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Text.RegularExpressions;

namespace e_commerce_service.common.Models.ModelExtensions {

    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    public class MediaInfos {
        public string FilePath { get; set; }

        public MediaTypes MediaType { get; set; }

        public VideoSource? Source {
            get {
                if (this.MediaType != MediaTypes.SHORT_VIDEO)
                    return null;

                if (this.FilePath.Contains(Constants.DOMAIN) || (!this.FilePath.StartsWith("https:") && !this.FilePath.StartsWith("http:")))
                    return VideoSource.UPLOAD_VUIVUI;
                return VideoSource.EXTERNAL_LINK;
            }
        }

        public string Domain {
            get {
                if (this.Source == VideoSource.UPLOAD_VUIVUI)
                    return string.Empty;
                var regex = new Regex(@"^((https?|ftp)://)?(www\.)?(?<domain>[^/]+)(/|$)");
                Match match = regex.Match(this.FilePath);
                if (match.Success) {
                    return match.Groups["domain"].Value.Split(".")[0];
                }
                return string.Empty;
            }
        }

        public int DisplayOrder { get; set; }
    }
}
