﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace e_commerce_service.common.Models.ModelExtensions {
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy), ItemNullValueHandling = NullValueHandling.Ignore)]
    public class ProductAttributeValue {
        public string AttributeValueId { get; set; }

        public string AttributeValueName { get; set; }

        /// <summary>
        /// key xác định giá trị thuộc tính có phải là giá trị khác hay ko
        /// </summary>
        public bool IsOther { get; set; }

        /// <summary>
        /// đường dẫn hình ảnh nếu attribute được chọn làm thuộc tính phân loại variation
        /// </summary>
        public string? ImagePath { get; set; }
    }
}
