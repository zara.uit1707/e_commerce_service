﻿using e_commerce_service.common.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace e_commerce_service.common.Models.ModelExtensions {
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy), ItemNullValueHandling = NullValueHandling.Ignore)]
    public class ProductAttribute {
        public string AttributeId { get; set; }

        public string AttributeName { get; set; }

        public List<ProductAttributeValue> AttributeValues { get; set; }

        public AttributeTypes Type { get; set; } = AttributeTypes.DEFAULT;

        /// <summary>
        /// key xác định thuộc tính có dùng để phân loại variation
        /// </summary>
        public bool IsCategoricalAttribute { get; set; } = false;

        /// <summary>
        /// key xác định thuộc tính phân loại(nếu có) có upload hình ảnh hay ko
        /// </summary>
        public bool? HadImageVariation { get; set; } = false;

        /// <summary>
        /// Thứ tự hiển thị
        /// </summary>
        public int DisplayOrder { get; set; } = 0;
    }
}
