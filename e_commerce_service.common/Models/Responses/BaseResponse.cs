﻿using e_commerce_service.common.Utilities;
using Newtonsoft.Json;
using System.Net;
using System.Runtime.Serialization;

namespace e_commerce_service.common.Models.Responses {
    public class BaseResponse<TResponse> {
        private HttpStatusCode Status { get; set; }

        [JsonProperty("header")] public object Header { get; set; }

        [JsonProperty("code")] public string Code { get; set; }

        [JsonProperty("subCode")] public string SubCode { get; set; }

        [DataMember(EmitDefaultValue = false)]
        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("data")] public TResponse Data { get; set; }

        [JsonProperty("isError")] public bool IsError => Code != ((int)HttpStatusCode.OK).ToString() && Message != null;

        [JsonProperty("totalObject")] public long TotalObject { get; set; }

        /// <summary>
        ///     Số lượng còn lại ngoài data đang trả về
        /// </summary>
        [JsonProperty("totalRemain")]
        public int? TotalRemain { get; set; }

        [JsonProperty("page")] public int Page { set; get; }

        [JsonProperty("pageSize")] public int PageSize { set; get; }

        [JsonProperty("isEmpty")] public bool? IsEmpty { set; get; }


        [JsonProperty("totalPage")]
        public long TotalPage {
            get {
                // ES quan ly page bat dau tu 0, nen se khong can tinh lai so Page
                long result = 1;
                //long residualObject = 0;
                if (PageSize != 0) {
                    result = (int)Math.Ceiling(TotalObject / (double)PageSize);
                }

                //residualObject = TotalObject % PageSize;

                //if (residualObject > 0) result++;

                return PageSize > 0 ? result : 0;
            }
        }

        public void UpdateStatusCode(HttpStatusCode statusCode) {
            Status = statusCode;
        }

        public int GetStatusCode() {
            var result = int.Parse(Code);

            return result;
        }

        public static BaseResponse<TResponse> Success(
            TResponse data = default,
            string message = "",
            int pageSize = 0,
            int page = 0,
            int? totalRemain = null,
            long? totalObject = 0,
            bool? isEmpty = null,
            string subCode = "") {
            if (string.IsNullOrEmpty(message)) message = "Success";

            return new BaseResponse<TResponse> {
                Status = HttpStatusCode.OK,
                Code = "200",
                IsEmpty = isEmpty,
                Data = data,
                Message = message,
                PageSize = pageSize,
                TotalRemain = totalRemain,
                Page = page,
                SubCode = subCode,
                TotalObject = totalObject == null ? 0 : totalObject.Value,
            };
        }

        public static BaseResponse<TResponse> SuccessWithDataOnDB(
            TResponse data = default,
            int pageSize = 0,
            int page = 0,
            int? totalRemain = 0,
            long totalObject = 0,
            string message = "") {
            if (string.IsNullOrEmpty(message)) message = "Success";

            return new BaseResponse<TResponse> {
                Status = HttpStatusCode.OK,
                Code = "200",
                Data = data,
                Message = message,
                PageSize = pageSize,
                Page = page,
                TotalObject = totalObject,
                TotalRemain = totalRemain,
            };
        }

        public static BaseResponse<TResponse> InternalServerError(
            Exception ex,
            TResponse data = default) {
            return new BaseResponse<TResponse> {
                Status = HttpStatusCode.InternalServerError,
                Code = "500",
                Data = data,
                Message = ex.CreateExceptionMessage()
            };
        }

        public static BaseResponse<TResponse> InternalServerError(
            TResponse data = default,
            string message = "Internal Server Error") {
            return new BaseResponse<TResponse> {
                Status = HttpStatusCode.InternalServerError,
                Code = "500",
                Data = data,
                Message = message
            };
        }

        public static BaseResponse<TResponse> BadRequest(
            TResponse data = default,
            string message = "Bad Request",
            object? header = null,
            string subCode = "") {
            return new BaseResponse<TResponse> {
                Status = HttpStatusCode.BadRequest,
                Code = "400",
                Data = data,
                Message = message,
                Header = header,
                SubCode = subCode
            };
        }
        public static BaseResponse<TResponse> Forbidden(
            TResponse data = default,
            string message = "Forbidden") {
            return new BaseResponse<TResponse> {
                Status = HttpStatusCode.NotFound,
                Code = "403",
                Data = data,
                Message = message
            };
        }

        public static BaseResponse<TResponse> NotFound(
            TResponse data = default,
            string message = "Data not found") {
            return new BaseResponse<TResponse> {
                Status = HttpStatusCode.NotFound,
                Code = "404",
                Data = data,
                Message = message
            };
        }

        public static BaseResponse<TResponse> NoContent(
            TResponse data = default,
            string message = "No Content") {
            return new BaseResponse<TResponse> {
                Status = HttpStatusCode.NoContent,
                Code = "204",
                Data = data,
                Message = message
            };
        }

        public static BaseResponse<TResponse> Unauthorized(
            string message = "User does not have permission merchant") {
            return new BaseResponse<TResponse> {
                Status = HttpStatusCode.Unauthorized,
                Code = "401",
                Message = message
            };
        }

        #region Json Validate

        public bool ShouldSerializeSubCode() {
            return string.IsNullOrEmpty(SubCode) == false;
        }

        public bool ShouldSerializeTotalObject() {
            return TotalObject > 0;
        }

        public bool ShouldSerializePage() {
            return Page > 0;
        }

        public bool ShouldSerializeTotalPage() {
            return TotalPage > 0;
        }

        public bool ShouldSerializePageSize() {
            return PageSize > 0;
        }

        public bool ShouldSerializeHeader() {
            return Header != null;
        }

        public bool ShouldSerializeIsEmpty() {
            return IsEmpty != null;
        }

        public bool ShouldSerializeTotalRemain() {
            return TotalRemain != null;
        }

        #endregion
    }
}
