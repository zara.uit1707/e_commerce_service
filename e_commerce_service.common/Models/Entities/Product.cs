﻿using e_commerce_service.common.Enums;
using e_commerce_service.common.Models.ModelExtensions;
using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_commerce_service.common.Models.Entities {
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    [Table("product")]
    public class Product : ModelBase {
        [Key()]
        [Column("id", TypeName = "varchar(36)")]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();

        [Column("product_name", TypeName = "varchar(100)")]
        public string ProductName { get; set; } //Tên sp

        [Column("product_content")]
        public string ProductContent { get; set; }  //Nội dung mô tả sp

        [Column("multi_media", TypeName = "jsonb")]
        public List<MediaInfos> MultiMedia { get; set; } //chứa info video/hình ảnh sp

        [Column("category_id", TypeName = "varchar(36)")]
        public string CategoryId { get; set; } //info ngành hàng

        [Column("brand_id", TypeName = "varchar(36)")]
        public string BrandId { get; set; } //info thương hiệu

        [Column("origin_id", TypeName = "varchar(36)")]
        public string OriginId { get; set; }  //info xuất xứ

        [Column("attributes", TypeName = "jsonb")]
        public List<ProductAttribute> Attributes { get; set; }  //info thuộc tính sp

        [Column("keyword")]
        public string Keyword { get; set; } //từ khóa gợi ý tìm kiếm sp

        [Column("hash_tag")]
        public List<string>? Hashtag { get; set; }   //list nhãn sp

        [Column("status")]
        public ProductStatus Status { get; set; }  //info trạng thái sp

        [Column("warranty", TypeName = "jsonb")]
        public WarrantyInfos Warrranty { get; set; }  //info bảo hành
    }
}
