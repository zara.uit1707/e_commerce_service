﻿using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_commerce_service.common.Models.Entities {
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    [Table("category")]
    public class Category : ModelBase {
        [Key()]
        [Column("id", TypeName = "varchar(36)")]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();

        [Column("name", TypeName = "varchar(100)")]
        public string Name { get; set; }

        [Column("image_path", TypeName = "varchar(150)")]
        public string? ImagePath { get; set; } = string.Empty;

        [Column("url_slug", TypeName = "varchar(100)")]
        public string UrlSlug { get; set; } = string.Empty;

        [Column("parent_id", TypeName = "varchar(36)")]
        public string? ParentId { get; set; }

        [Column("has_children")]
        public bool HasChildren { get; set; }

        [Column("description")]
        public string? Description { get; set; } = string.Empty;

        /// <summary>
        /// Danh sách từ khóa của Ngành hàng
        /// </summary>
        [Column("hash_tag", TypeName = "jsonb")]
        public List<string>? Hashtag { get; set; } = new List<string>();

        [Column("short_name", TypeName = "varchar(100)")]
        public string ShortName { get; set; }

        [Column("is_actived")]
        public bool IsActived { get; set; } = true;
    }
}
