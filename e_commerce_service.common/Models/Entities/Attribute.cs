﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using MongoDB.Bson;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using e_commerce_service.common.Enums;

namespace e_commerce_service.common.Models.Entities {
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    [Table("attribute")]
    public class Attribute : ModelBase {
        [Key()]
        [Column("id", TypeName = "varchar(36)")]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();

        [Column("name", TypeName = "varchar(100)")]
        public string Name { get; set; }

        [Column("display_order")]
        public int DisplayOrder { get; set; }

        /// <summary>
        /// key check thuộc tính có bắt buộc hay ko
        /// </summary>
        [Column("is_mandatory")]
        public bool IsMandatory { get; set; }

        [Column("type")]
        public AttributeTypes Type { get; set; } = AttributeTypes.DEFAULT;

        [Column("is_actived")]
        public bool IsActived { get; set; } = true;
    }
}
