﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_commerce_service.common.Models.Entities {
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy), ItemNullValueHandling = NullValueHandling.Ignore)]
    public class ModelBase {
        [JsonProperty(Order = 79)]
        [Column("created_at")]
        public DateTime CreatedAt { get; set; } = Constants.DATE_TIME_UTC_NOW;

        [JsonProperty(Order = 79)]
        [Column("created_by")]
        public Guid? CreatedBy { get; set; }

        [JsonProperty(Order = 79)]
        [Column("updated_at")]
        public DateTime? UpdatedAt { get; set; }

        [JsonProperty(Order = 79)]
        [Column("updated_by")]
        public Guid? UpdatedBy { get; set; }
    }
}
