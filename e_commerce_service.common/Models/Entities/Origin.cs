﻿using e_commerce_service.common.Utilities;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_commerce_service.common.Models.Entities {
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    [Table("origin")]
    public class Origin : ModelBase {
        [Key()]
        [Column("id", TypeName = "varchar(36)")]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();

        [Column("name", TypeName = "varchar(100)")]
        public string Name { get; set; }

        [Column("name_en", TypeName = "varchar(100)")]
        public string NameEN => Name.NullToEmpty().CyrillicToASCII().GetValueWithoutSpecialCharacters("_");

        [Column("is_actived")]
        public bool IsActived { get; set; } = true;
    }
}
