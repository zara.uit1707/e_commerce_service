﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_commerce_service.common.Models.Entities {
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    [Table("variation")]
    public class Variation : ModelBase {
        [Key()]
        [Column("id", TypeName = "varchar(36)")]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();

        [Column("product_id", TypeName = "varchar(36)")]
        public string? ProductId { get; set; }

        [Column("sku", TypeName = "varchar(50)")]
        public string? Sku { get; set; }

        [Column("original_price")]
        public long OriginalPrice { get; set; }

        [Column("quantity")]
        public int Quantity { get; set; }


        /// <summary>
        /// Thứ tự AttributeValue trong ds thuộc tính phân loại thứ 2
        /// </summary>
        [Column("index_primary")]
        public int? IndexPrimary { get; set; }

        /// <summary>
        /// Id thuộc tính phân loại 1 nếu lấy từ bộ thuộc tính
        /// </summary>
        [Column("attribute_id_primary", TypeName = "varchar(36)")]
        public string? AttributeIdPrimary { get; set; }

        /// <summary>
        /// Tên thuộc tính phân loại 1
        /// </summary>
        [Column("attribute_name_primary", TypeName = "varchar(100)")]
        public string? AttributeNamePrimary { get; set; }

        /// <summary>
        /// Tên giá trị thuộc tính phân loại 1
        /// </summary>
        [Column("attribute_value_name_primary", TypeName = "varchar(100)")]
        public string? AttributeValueNamePrimary { get; set; }


        /// <summary>
        /// Thứ tự AttributeValue trong ds thuộc tính phân loại thứ 2
        /// </summary>
        [Column("index_secondary")]
        public int? IndexSecondary { get; set; }

        /// <summary>
        /// Id thuộc tính phân loại 2 nếu lấy từ bộ thuộc tính
        /// </summary>
        [Column("attribute_id_secondary", TypeName = "varchar(36)")]
        public string? AttributeIdSecondary { get; set; }

        /// <summary>
        /// Tên thuộc tính phân loại 2
        /// </summary>
        [Column("attribute_name_secondary", TypeName = "varchar(100)")]
        public string? AttributeNameSecondary { get; set; }

        /// <summary>
        /// Tên giá trị thuộc tính phân loại 2
        /// </summary>
        [Column("attribute_value_name_secondary", TypeName = "varchar(100)")]
        public string? AttributeValueNameSecondary { get; set; }


        /// <summary>
        /// Trạng thái bật/tắt kinh doanh variation
        /// </summary>
        [Column("is_enabled")]
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Trạng thái hoạt động của variation trong product
        /// </summary>
        [Column("is_actived")]
        public bool IsActived { get; set; }
    }
}
