﻿using MongoDB.Bson;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace e_commerce_service.common.Models.Entities {
    [JsonObject(NamingStrategyType = typeof(CamelCaseNamingStrategy))]
    [Table("origin_category")]
    public class OriginCategory {
        [Key()]
        [Column("id", TypeName = "varchar(36)")]
        public string Id { get; set; } = ObjectId.GenerateNewId().ToString();

        [Column("brand_id", TypeName = "varchar(36)")]
        public string BrandId { get; set; }

        [Column("category_id", TypeName = "varchar(36)")]
        public string CategoryId { get; set; }

        [Column("is_actived")]
        public bool IsActived { get; set; } = true;
    }
}
