﻿namespace e_commerce_service.common.Enums {
    public enum VideoSource {

        /// <summary>
        ///Nguồn video từ server VuiVui (được upload lên server VuiVui)
        /// </summary>
        UPLOAD_VUIVUI = 1,
        /// <summary>
        /// Link video từ nguồn bên ngoài như youtube, facebook, tiktok,...
        /// </summary>
        EXTERNAL_LINK = 2,
    }

    public enum MediaTypes {
        /// <summary>
        /// Ảnh bìa hoặc ảnh đại diện
        /// </summary>
        AVATAR_OR_COVER_IMAGE = 1,
        /// <summary>
        /// Hình ảnh nổi bật, điểm nhấn sp
        /// </summary>
        FEATURED_INFORMATIONAL_IMAGE = 2,
        /// <summary>
        /// Video ngắn
        /// </summary>
        SHORT_VIDEO = 3,
        /// <summary>
        /// Khác
        /// </summary>
        OTHER = 10
    }

    public enum ProductStatus {
        /// <summary>
        /// sp phác thảo, sp nháp
        /// </summary>
        DRAFT = 1,
        /// <summary>
        /// Chưa kiểm duyệt, trình kiểm duyệt, chờ duyệt
        /// </summary>
        REVIEWING = 2,
        /// <summary>
        /// sp phát hành, sp đang hoạt động
        /// </summary>
        PUBLISHED = 3,
        /// <summary>
        /// sp tạm ngưng
        /// </summary>
        PAUSING = 4,
        /// <summary>
        /// sp bị khóa
        /// </summary>
        LOCKED = 5,
        /// <summary>
        /// sp bị xóa
        /// </summary>
        REMOVE = 6
    }

    public enum WarrantyForm {
        /// <summary>
        /// Tin nhắn điện thoại/ Email
        /// </summary>
        SMS_EMAIL = 1,
        /// <summary>
        /// Mã đơn hàng
        /// </summary>
        CODE_ORDERS = 2,
        /// <summary>
        /// Giấy bảo hành
        /// </summary>
        WARRANTY_PAPER = 3
    }

    public enum AttributeTypes {
        DEFAULT = 1,
        COLOR = 2,
        FILTER = 3
    }
}
