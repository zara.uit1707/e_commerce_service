﻿namespace e_commerce_service.common.Services.Entities.Interfaces {
    public interface ITestBenchMarkService {
        Task SetupData();
        Task UpdateEF_OldStyle();
        Task UpdateEF_Core7();
    }
}
