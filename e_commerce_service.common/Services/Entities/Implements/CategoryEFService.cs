﻿using e_commerce_service.common.DBContext;
using e_commerce_service.common.Models.Entities;
using e_commerce_service.common.Models.Responses;
using e_commerce_service.common.Services.Entities.Interfaces;
using e_commerce_service.common.Utilities;
using Microsoft.EntityFrameworkCore;

namespace e_commerce_service.common.Services.Entities.Implements {
    public class CategoryEFService : ICategoryEFService {
        private readonly ECommerceDbContext _context;

        public CategoryEFService(ECommerceDbContext context) {
            _context = context;
        }

        public async Task<BaseResponse<Category>> GetByIdAsync(string id) {
            BaseResponse<Category> response = new();
            try {
                using var context = _context;
                var result = await context.Categories.FirstOrDefaultAsync(f => f.Id.Equals(id));
                if (result != null) {
                    response.Data = result;
                }
                else {
                    response.Message = "Không tìm thấy dữ liệu với id truyền vào.";
                }
            }
            catch (Exception ex) {
                response.Message = ex.CreateExceptionMessage();
            }
            return response;
        }

        public async Task<BaseResponse<List<Category>>> SearchAsync(string content) {
            BaseResponse<List<Category>> response = new();
            try {
                using var context = _context;
                var result = await context.Categories
                                .Where(p => p.Name.Contains(content))
                                .ToListAsync();
                response.Data = result;
            }
            catch (Exception ex) {
                response.Message = ex.CreateExceptionMessage();
            }
            return response;
        }

        public async Task<BaseResponse<bool>> InsertAsync(Category model) {
            BaseResponse<bool> response = new();
            try {
                using var context = _context;
                context.Add(model);
                var result = await context.SaveChangesAsync();
                if (result > 0) {
                    response.Data = true;
                }
                else {
                    response.Data = false;
                    response.Message = "Có lỗi xảy ra trong quá trình insert data vào DB.";
                }
            }
            catch (Exception ex) {
                response.Message = ex.CreateExceptionMessage();
                response.Data = false;
            }
            return response;
        }

        public async Task<BaseResponse<bool>> InsertAsync(List<Category> models) {
            BaseResponse<bool> response = new();
            try {
                using var context = _context;
                using var transaction = context.Database.BeginTransaction();

                context.AddRange(models);
                var result = await context.SaveChangesAsync();
                if (result > 0) {
                    response.Data = true;
                    transaction.Commit();
                }
                else {
                    response.Data = false;
                    response.Message = "Có lỗi xảy ra trong quá trình insert data vào DB.";
                }
            }
            catch (Exception ex) {
                response.Message = ex.CreateExceptionMessage();
                response.Data = false;
            }
            return response;
        }

        public async Task<BaseResponse<bool>> UpdateAsync(Category model) {
            BaseResponse<bool> response = new();
            try {
                using var context = _context;
                var result = await context.Categories
                    .Where(predicate => predicate.Id == model.Id)
                    .ExecuteUpdateAsync(ex => ex
                        .SetProperty(p => p.Name, model.Name)
                        .SetProperty(p => p.Description, model.Description)
                        .SetProperty(p => p.ImagePath, model.ImagePath)
                        .SetProperty(p => p.UrlSlug, model.UrlSlug)
                        .SetProperty(p => p.ParentId, model.ParentId)
                        .SetProperty(p => p.HasChildren, model.HasChildren)
                        .SetProperty(p => p.Description, model.Description)
                        .SetProperty(p => p.Hashtag, model.Hashtag)
                        .SetProperty(p => p.ShortName, model.ShortName)
                        .SetProperty(p => p.IsActived, model.IsActived)
                );
                if (result > 0) {
                    response.Data = true;
                }
                else {
                    response.Data = false;
                    response.Message = "Có lỗi xảy ra trong quá trình insert data vào DB.";
                }
            }
            catch (Exception ex) {
                response.Message = ex.CreateExceptionMessage();
                response.Data = false;
            }
            return response;
        }

        public async Task<BaseResponse<bool>> ChangeActiveAsync(string id, bool status) {
            BaseResponse<bool> response = new();
            try {
                using var context = _context;
                var result = await context.Categories
                            .Where(predicate => predicate.Id == id)
                            .ExecuteUpdateAsync(e => e
                                .SetProperty(s => s.IsActived, status)
                            );
                response.Data = result > 0;
            }
            catch (Exception ex) {
                response.Message = ex.CreateExceptionMessage();
                response.Data = false;
            }
            return response;
        }
    }
}
