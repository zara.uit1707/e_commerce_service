﻿//using BenchmarkDotNet.Attributes;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.EntityFrameworkCore.Infrastructure;
//using e_commerce_service.common.DBContext;
//using e_commerce_service.common.Services.Entities.Interfaces;
//using System.Diagnostics;

//namespace e_commerce_service.common.Services.Entities.Implements
//{
//    [MemoryDiagnoser]
//    public class TestBenchMarkService : ITestBenchMarkService
//    {
//        private int _counter = 10000;
//        private PooledDbContextFactory<ECommerceDbContext> _contextFactory = null;

//        public TestBenchMarkService()
//        {
//        }

//        [GlobalSetup]
//        public async Task SetupData()
//        {
//            //List<Blog> blogs = new List<Blog>();
//            //for (int i = 1; i <= _counter; i++)
//            //{
//            //    blogs.Add(new Blog { Name = String.Format("Blog-{0}", i), Rating = i, IsActived = true });
//            //}
//            //_context.Blogs.AddRange(blogs);
//            //await _context.SaveChangesAsync();
//            var optionsBuilder = new DbContextOptionsBuilder<ECommerceDbContext>();
//            optionsBuilder.UseNpgsql(Constants.PSQL_CONNECTIONSTRING);
//            _contextFactory = new PooledDbContextFactory<ECommerceDbContext>(optionsBuilder.Options);

//            using var context = _contextFactory.CreateDbContext();
//            await context.Blogs
//                .ExecuteUpdateAsync(updates => updates
//                    .SetProperty(p => p.Counter, 0)
//                );
//        }

//        [Benchmark]
//        public async Task UpdateEF_OldStyle()
//        {
//            var watch = new Stopwatch();
//            watch.Start();
//            using var context = _contextFactory.CreateDbContext();
//            var blogs = await context.Blogs.Where(p => p.Id % 2 == 0).ToListAsync();

//            foreach (var item in blogs)
//            {
//                item.IsActived = true;
//                item.Counter += 1;
//            }

//            await context.SaveChangesAsync();
//            watch.Stop();
//            Console.WriteLine(string.Format("UpdateEF_OldStyle runtime: {0}", watch.ElapsedMilliseconds));
//        }

//        [Benchmark]
//        public async Task UpdateEF_Core7()
//        {
//            var watch = new Stopwatch();
//            watch.Start();
//            using var context = _contextFactory.CreateDbContext();
//            var blogs = await context.Blogs
//                .Where(x => x.Id % 2 == 1)
//                .ExecuteUpdateAsync(updates => updates
//                    .SetProperty(p => p.IsActived, true)
//                    .SetProperty(p => p.Counter, p => p.Counter + 1)
//                );
//            watch.Stop();
//            Console.WriteLine(string.Format("UpdateEF_Core7 runtime: {0}", watch.ElapsedMilliseconds));
//        }
//    }
//}
