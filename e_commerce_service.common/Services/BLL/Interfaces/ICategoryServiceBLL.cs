﻿using e_commerce_service.common.Models.Entities;
using e_commerce_service.common.Models.Responses;

namespace e_commerce_service.common.Services.BLL.Interfaces {
    public interface ICategoryServiceBLL {
        Task<BaseResponse<Category>> GetByIdAsync(string id);

        Task<BaseResponse<List<Category>>> SearchAsync(string content);

        Task<BaseResponse<bool>> InsertAsync(Category model);

        Task<BaseResponse<bool>> InsertAsync(List<Category> models);

        Task<BaseResponse<bool>> UpdateAsync(string id, Category model);

        Task<BaseResponse<bool>> ChangeActiveAsync(string id, bool status);
    }
}
