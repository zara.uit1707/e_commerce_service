﻿using e_commerce_service.common.Models.Entities;
using e_commerce_service.common.Models.Responses;
using e_commerce_service.common.Services.BLL.Interfaces;
using e_commerce_service.common.Services.Entities.Interfaces;
using e_commerce_service.common.Utilities;

namespace e_commerce_service.common.Services.BLL.Implements {
    public class CategoryServiceBLL : ICategoryServiceBLL {
        private ICategoryEFService _categoryEFService;

        public CategoryServiceBLL(ICategoryEFService categoryEFService) {
            _categoryEFService = categoryEFService;
        }

        public async Task<BaseResponse<Category>> GetByIdAsync(string id) {
            try {
                var result = await _categoryEFService.GetByIdAsync(id);
                if (result.IsError) {
                    return BaseResponse<Category>.InternalServerError(data: result.Data, message: result.Message);
                }
                return BaseResponse<Category>.Success(data: result.Data);
            }
            catch (Exception ex) {
                return BaseResponse<Category>.InternalServerError(message: ex.CreateExceptionMessage());
            }
        }

        public async Task<BaseResponse<List<Category>>> SearchAsync(string content) {
            try {
                var result = await _categoryEFService.SearchAsync(content);
                if (result.IsError) {
                    return BaseResponse<List<Category>>.InternalServerError(data: result.Data, message: result.Message);
                }
                return BaseResponse<List<Category>>.Success(data: result.Data);
            }
            catch (Exception ex) {
                return BaseResponse<List<Category>>.InternalServerError(message: ex.CreateExceptionMessage());
            }
        }

        public async Task<BaseResponse<bool>> InsertAsync(Category model) {
            try {
                var result = await _categoryEFService.InsertAsync(model);
                if (result.IsError) {
                    return BaseResponse<bool>.InternalServerError(data: result.Data, message: result.Message);
                }
                return BaseResponse<bool>.Success(data: result.Data);
            }
            catch (Exception ex) {
                return BaseResponse<bool>.InternalServerError(data: false, message: ex.CreateExceptionMessage());
            }
        }

        public async Task<BaseResponse<bool>> InsertAsync(List<Category> models) {
            try {
                var result = await _categoryEFService.InsertAsync(models);
                if (result.IsError) {
                    return BaseResponse<bool>.InternalServerError(data: result.Data, message: result.Message);
                }
                return BaseResponse<bool>.Success(data: result.Data);
            }
            catch (Exception ex) {
                return BaseResponse<bool>.InternalServerError(data: false, message: ex.CreateExceptionMessage());
            }
        }

        public async Task<BaseResponse<bool>> UpdateAsync(string id, Category model) {
            try {
                if (id.Equals(model.Id)) {
                    var result = await _categoryEFService.UpdateAsync(model);
                    if (result.IsError) {
                        return BaseResponse<bool>.InternalServerError(data: result.Data, message: result.Message);
                    }
                    return BaseResponse<bool>.Success(data: result.Data);
                }
                else {
                    return BaseResponse<bool>.BadRequest(data: false, message: "Thông tin Id truyền vào không đồng nhất.");
                }
            }
            catch (Exception ex) {
                return BaseResponse<bool>.InternalServerError(data: false, message: ex.CreateExceptionMessage());
            }
        }

        public async Task<BaseResponse<bool>> ChangeActiveAsync(string id, bool status) {
            try {
                var result = await _categoryEFService.ChangeActiveAsync(id, status);
                if (result.IsError) {
                    return BaseResponse<bool>.InternalServerError(data: result.Data, message: result.Message);
                }
                return BaseResponse<bool>.Success(data: result.Data);
            }
            catch (Exception ex) {
                return BaseResponse<bool>.InternalServerError(data: false, message: ex.CreateExceptionMessage());
            }
        }
    }
}
