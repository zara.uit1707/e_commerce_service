﻿using e_commerce_service.common.DBContext;
using e_commerce_service.common.Services.BLL.Implements;
using e_commerce_service.common.Services.BLL.Interfaces;
using e_commerce_service.common.Services.Entities.Implements;
using e_commerce_service.common.Services.Entities.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace e_commerce_service.common {
    public static class ServiceConfiguration {
        private static readonly ConfigUtilities _commonConfig = new ConfigUtilities("e_commerce_service_setting.json");

        public static readonly string CATALOG_DB = _commonConfig.Configuration.GetConnectionString("PSQL_Connection")!;

        public static void AddCommonServiceConfig(
            this IServiceCollection services,
            IConfiguration configuration,
            string fullName) {
            Constants.PSQL_CONNECTIONSTRING = _commonConfig.Configuration.GetConnectionString("PSQL_Connection")!;

            services.AddControllersJson();

            //Add Entity Framework with PostgreSQL
            services.AddDbContextPool<ECommerceDbContext>(builder => {
                builder.UseNpgsql(Constants.PSQL_CONNECTIONSTRING, migration => migration.MigrationsAssembly(fullName));
            }).AddTransient<IECommerceDbContext, ECommerceDbContext>();

            //Khởi tạo Dependency Injection(DI)
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<ICategoryEFService, CategoryEFService>();
            services.AddScoped<ICategoryServiceBLL, CategoryServiceBLL>();
        }

        private static void AddControllersJson(this IServiceCollection services) {
            services.AddControllers().AddJsonOptions(options => {
                options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve;
                options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
            }).AddNewtonsoftJson(options => {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
        }

    }
}