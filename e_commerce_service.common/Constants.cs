﻿namespace e_commerce_service.common;
public static class Constants {
    public static string PSQL_CONNECTIONSTRING { get; set; } = string.Empty;

    public static string DOMAIN = @"https://testcdn.ecommerce.com";

    public static DateTime DATE_TIME_UTC_NOW => DateTime.Now;
}
