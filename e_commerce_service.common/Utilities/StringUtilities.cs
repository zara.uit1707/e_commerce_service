﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace e_commerce_service.common.Utilities {
    public static class StringUtilities {
        private static readonly Random _random = new();

        private static readonly int[] _UnicodeCharactersList =
            Enumerable.Range(48, 10) // Numbers           48   - 57
                .Concat(Enumerable.Range(65, 26)) // English uppercase 65   - 90
                .ToArray();

        /// <summary>
        /// ký tự mà máy MAC hay bị lỗi
        /// </summary>
        private static readonly List<KeyValuePair<string, string>> _cyrillicCharacters = new List<KeyValuePair<string, string>>
        {
        new KeyValuePair<string, string>("\\u0400", "E"),
        new KeyValuePair<string, string>("\\u0401", "E"),
        new KeyValuePair<string, string>("\\u0405", "S"),
        new KeyValuePair<string, string>("\\u0406", "I"),
        new KeyValuePair<string, string>("\\u0407", "I"),
        new KeyValuePair<string, string>("\\u0408", "J"),
        new KeyValuePair<string, string>("\\u040E", "Y"),
        new KeyValuePair<string, string>("\\u0410", "A"),
        new KeyValuePair<string, string>("\\u0412", "B"),
        new KeyValuePair<string, string>("\\u0415", "E"),
        new KeyValuePair<string, string>("\\u041A", "K"),
        new KeyValuePair<string, string>("\\u041C", "M"),
        new KeyValuePair<string, string>("\\u041D", "H"),
        new KeyValuePair<string, string>("\\u041E", "O"),
        new KeyValuePair<string, string>("\\u0420", "P"),
        new KeyValuePair<string, string>("\\u0421", "C"),
        new KeyValuePair<string, string>("\\u0422", "T"),
        new KeyValuePair<string, string>("\\u0423", "Y"),
        new KeyValuePair<string, string>("\\u0425", "X"),
        new KeyValuePair<string, string>("\\u0432", "B"),
        new KeyValuePair<string, string>("\\u0433", "R"),
        new KeyValuePair<string, string>("\\u043A", "K"),
        new KeyValuePair<string, string>("\\u043C", "M"),
        new KeyValuePair<string, string>("\\u043D", "H"),
        new KeyValuePair<string, string>("\\u043E", "O"),
        new KeyValuePair<string, string>("\\u0440", "P"),
        new KeyValuePair<string, string>("\\u0441", "C"),
        new KeyValuePair<string, string>("\\u0442", "T"),
        new KeyValuePair<string, string>("\\u0443", "Y"),
        new KeyValuePair<string, string>("\\u0445", "X")
    };

        public static string CyrillicToASCII(this string value) {
            if (string.IsNullOrEmpty(value))
                return string.Empty;

            var encoded = EncodeNonAsciiCharacters(value);

            var decoded = DecodeEncodedNonAsciiCharacters(encoded);

            return decoded;
        }

        private static string DecodeEncodedNonAsciiCharacters(string value) {
            var result = Regex.Replace(
                value,
                @"\\u(?<Value>[a-zA-Z0-9]{4})",
                match => {
                    var value = match.Groups[0].Value;
                    var data = _cyrillicCharacters.FirstOrDefault(x => x.Key.ToLower() == value.ToLower());

                    return data.Value.NullToEmpty();
                });

            return result;
        }

        private static string EncodeNonAsciiCharacters(string value) {
            return Regex.Replace(
              value,
              @"[^\x00-\x7F]",
              m => String.Format("\\u{0:X4}", (int)m.Value[0]));
        }

        public static string GenerateOTP() {
            var token = new Random().Next(0, 999999).ToString("000000");

            return token;
        }

        public static string GetKeywordElasticFullTextSearch(this string value) {
            var invalidChars = Path.GetInvalidFileNameChars();
            string invalidCharsRemoved = new string(value
                .Select(x => {
                    if (!invalidChars.Contains(x)) return x;
                    return ' ';
                })
                .ToArray());
            var result = string.Join(
                " ",
                invalidCharsRemoved.Split(' ').Where(x => !string.IsNullOrEmpty(x)).Select(item => string.Format("*{0}*", item)));

            return result;
        }

        public static string GetKeywordElasticFullTextSearchV2(this string value) {
            var invalidChars = Path.GetInvalidFileNameChars();
            string invalidCharsRemoved = new string(value
                .Select(x => {
                    if (!invalidChars.Contains(x)) return x;
                    return ' ';
                })
                .ToArray());
            var arrWildcard = invalidCharsRemoved.Split(' ').Where(x => !string.IsNullOrEmpty(x)).ToArray();
            if (arrWildcard.Length > 0)
                arrWildcard[0] = arrWildcard[0] + "^2";
            value = String.Join(" ", arrWildcard);
            var temp = "";
            for (var i = 0; i < arrWildcard.Length; i++)
                if (i < arrWildcard.Length - 1)
                    temp = temp + arrWildcard[i] + " AND ";
                else
                    temp = temp + arrWildcard[i];

            var result = "(*" + value + ") OR " +
                         "(" + value + "*) OR " +
                         "(" + temp + ")";

            return result;
        }

        public static string GetKeywordElasticFullTextSearchV3(this string value) {
            var invalidChars = Path.GetInvalidFileNameChars();
            string invalidCharsRemoved = new string(value
                .Select(x => {
                    if (!invalidChars.Contains(x)) return x;
                    return ' ';
                })
                .ToArray());

            var result = string.Join(
                " AND ",
                invalidCharsRemoved.Split(' ').Where(x => !string.IsNullOrEmpty(x)));

            return result;
        }

        public static string GetKeywordElasticSearchLike(this string value) {
            var invalidChars = Path.GetInvalidFileNameChars();
            string invalidCharsRemoved = new string(value
                .Select(x => {
                    if (!invalidChars.Contains(x)) return x;
                    return ' ';
                })
                .ToArray());
            var arrText = invalidCharsRemoved.Split(' ').Where(x => !string.IsNullOrEmpty(x)).Select(item => string.Format("*{0}*", item)).ToArray();
            var result = string.Join(
                " AND ", arrText);

            return result;
        }

        public static string GetValueWithoutSpecialCharacters(
            this string value,
            string separatorCharacter = "") {
            var patternRegex = @"[^a-zA-Z0-9_]+";
            var result = Regex.Replace(value.UnicodeToASCII(), patternRegex, separatorCharacter);

            return result.ToLower();
        }

        public static string NullToEmpty(this object value) {
            if (value == null || value == DBNull.Value) return string.Empty;

            return value.ToString().Trim();
        }

        public static string RandomAppId(int length = 8) {
            Random random = new Random();
            const string chars = "ABCDEF0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public static string GenarateRandomString(int maxSize = 12) {
            // Step 1: Random number
            var randomNumberWithDate = new Random((int)DateTime.Now.Ticks)
                .Next(1, 1000)
                .ToString();

            // Step 2: Random difference sequence
            var differenceString = string.Empty;

            var difference = maxSize - randomNumberWithDate.Length;
            for (var i = 0; i < difference; i++) {
                var randomCharacter = _UnicodeCharactersList[
                    _random.Next(1, _UnicodeCharactersList.Length)];

                differenceString += Convert.ToChar(randomCharacter);
            }

            // Step 3: Insert the string in Step 1 anywhere from the string in Step 2
            var index = new Random().Next(0, differenceString.Length);
            var result = differenceString
                .Insert(index, randomNumberWithDate);

            return result;
        }

        public static string UnicodeToASCII(this string value) {
            if (string.IsNullOrEmpty(value))
                return string.Empty;

            var regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            var temp = value.Normalize(NormalizationForm.FormD);

            return regex.Replace(temp, string.Empty).Replace('đ', 'd').Replace('Đ', 'D');
        }

        public static string CreateMD5(this Stream stream) {
            // Use input string to calculate MD5 hash
            using (var create = MD5.Create()) {
                var hashBytes = create.ComputeHash(stream);

                // Convert the byte array to hexadecimal string
                var builder = new StringBuilder();
                for (var i = 0; i < hashBytes.Length; i++) builder.Append(hashBytes[i].ToString("X2").ToLower());

                return builder.ToString();
            }
        }

        public static string Truncate(this string value, int maxLength) {
            if (!string.IsNullOrEmpty(value))
                return value.Substring(0, Math.Min(value.Length, maxLength));

            return value;
        }

        public static string CreateMD5(this string input) {
            // Use input string to calculate MD5 hash
            using (var md5 = MD5.Create()) {
                var inputBytes = Encoding.ASCII.GetBytes(input);
                var hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                var builder = new StringBuilder();
                for (var i = 0; i < hashBytes.Length; i++) builder.Append(hashBytes[i].ToString("X2").ToLower());

                return builder.ToString();
            }
        }

        /// <summary>
        ///     Provides a forwarding func for JWT vs reference tokens (based on existence of dot in token)
        /// </summary>
        /// <param name="introspectionScheme">Scheme name of the introspection handler</param>
        /// <returns></returns>
        //public static Func<HttpContext, string> ForwardReferenceToken(
        //    string authenticationScheme,
        //    string introspectionScheme = Constants.AUTHENTICATION_SCHEME_REFERENCE) {
        //    string Select(HttpContext context) {
        //        var (scheme, credential) = GetSchemeAndCredential(context);

        //        if (scheme.Equals(authenticationScheme, StringComparison.OrdinalIgnoreCase)
        //            && !credential.Contains("."))
        //            return introspectionScheme;

        //        return null;
        //    }

        //    return Select;
        //}

        /// <summary>
        ///     Extracts scheme and credential from Authorization header (if present)
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public static (string, string) GetSchemeAndCredential(HttpContext context) {
            var header = context.Request.Headers["Authorization"].FirstOrDefault();

            if (string.IsNullOrEmpty(header)) return (string.Empty, string.Empty);

            var parts = header.Split(' ', StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2) return (string.Empty, string.Empty);

            return (parts[0], parts[1]);
        }
        public static int ParseString2Int(this string str) {
            int output = 0;
            Int32.TryParse(str, out output);
            return output;
        }
        public static bool ParseString2Bool(this string str) {
            Boolean output;
            Boolean.TryParse(str, out output);
            return output;
        }
        public static decimal ParseString2Decimal(string str) {
            decimal output = 0;
            Decimal.TryParse(str, out output);
            return output;
        }
        public static double ParseString2Double(this string value) {
            double outVal = 0;
            if (!String.IsNullOrEmpty(value)) {

                double.TryParse(value, out outVal);

                if (double.IsNaN(outVal) || double.IsInfinity(outVal)) {
                    return 0;
                }
                return outVal;
            }

            return outVal;
        }

        private static readonly string[] VietnameseSigns = new string[]
        {
        "aAeEoOuUiIdDyY",
        "áàạảãâấầậẩẫăắằặẳẵ",
        "ÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴ",
        "éèẹẻẽêếềệểễ",
        "ÉÈẸẺẼÊẾỀỆỂỄ",
        "óòọỏõôốồộổỗơớờợởỡ",
        "ÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠ",
        "úùụủũưứừựửữ",
        "ÚÙỤỦŨƯỨỪỰỬỮ",
        "íìịỉĩ",
        "ÍÌỊỈĨ",
        "đ",
        "Đ",
        "ýỳỵỷỹ",
        "ÝỲỴỶỸ"
        };
        public static string RemoveSign4VietnameseString(string str) {
            for (int i = 1; i < VietnameseSigns.Length; i++) {
                for (int j = 0; j < VietnameseSigns[i].Length; j++)
                    str = str.Replace(VietnameseSigns[i][j], VietnameseSigns[0][i - 1]);
            }
            return str;
        }

        public static string GetRealIP(this IHeaderDictionary headers, string key) {
            var dataHeader = headers
                 .ToDictionary(item => item.Key.ToLower(), item => item.Value);

            var result = string.Empty;
            if (dataHeader.ContainsKey(key.ToLower()) == false) {
                return string.Empty;
            }

            result = dataHeader[key.ToLower()].NullToEmpty();

            return result;
        }

        public static string MaskString(this string source, int start, int maskLength, char maskCharacter = '*') {
            if (source == null || source.Length <= 0 || maskLength < 0) return "";
            string mask = new string(maskCharacter, maskLength);
            string unMaskStart = source.Substring(0, start);
            string unMaskEnd = source.Substring(start + maskLength, source.Length - maskLength);
            return unMaskStart + mask + unMaskEnd;
        }

        public static T ConvertQueryStringToObject<T>(this string fromObject) {
            var dict = HttpUtility.ParseQueryString(fromObject);
            string json = JsonConvert.SerializeObject(dict.Cast<string>().ToDictionary(k => k, v => dict[v]));
            var result = JsonConvert.DeserializeObject<T>(json);
            return result;
        }

        public static T ToObject<T>(this Object fromObject) {
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(fromObject, new JsonSerializerSettings {
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore
            }));
        }

        public static List<T> ToObjectList<T>(this Object fromObject) {
            return JsonConvert.DeserializeObject<List<T>>(JsonConvert.SerializeObject(fromObject, new JsonSerializerSettings {
                NullValueHandling = NullValueHandling.Ignore,
                DefaultValueHandling = DefaultValueHandling.Ignore
            }));
        }

        public static string ToSnakeCase(this string text) {
            if (text == null) {
                throw new ArgumentNullException(nameof(text));
            }
            if (text.Length < 2) {
                return text;
            }
            var sb = new StringBuilder();
            sb.Append(char.ToLowerInvariant(text[0]));
            for (int i = 1; i < text.Length; ++i) {
                char c = text[i];
                if (char.IsUpper(c)) {
                    sb.Append('_');
                    sb.Append(char.ToLowerInvariant(c));
                }
                else {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string RemoveDomainName(this string Url, string domainTarget = null) {
            var regex = new Regex(@"^((https?|ftp)://)?(www\.)?(?<domain>[^/]+)(/|$)");
            Match match = regex.Match(Url);
            if (match.Success) {
                string domain = match.Groups["domain"].Value;
                int freq = domain.Where(x => (x == '.')).Count();
                while (freq > 2) {
                    if (freq > 2) {
                        var domainSplited = domain.Split('.', 2);
                        domain = domainSplited[1];
                        freq = domain.Where(x => (x == '.')).Count();
                    }
                }
                if (string.IsNullOrEmpty(domainTarget))
                    return Url.Remove(0, match.Value.Count() - 1);
                else {
                    Match matchDomainTarget = regex.Match(domainTarget);
                    if (!matchDomainTarget.Success)
                        return string.Empty;
                }

                if (domainTarget.Contains(domain))
                    return Url.Remove(0, match.Value.Count() - 1);
                else
                    return Url;
            }
            else {
                return String.Empty;
            }
        }

        public static string ReplaceContent(this string content, Dictionary<string, string> input) {
            Regex _regexParameter = new("@@.*?(.*?)@@");
            var result = _regexParameter.Replace(content, delegate (Match match) {
                var dataKey = match.Groups.Values.LastOrDefault()?.Value.NullToEmpty();
                var result = string.IsNullOrEmpty(dataKey)
                    ? string.Empty
                    : input.ContainsKey(dataKey)
                        ? input[dataKey]
                        : string.Empty;

                return string.IsNullOrEmpty(result) ? match.Value : result;
            });

            return result;
        }
    }
}
