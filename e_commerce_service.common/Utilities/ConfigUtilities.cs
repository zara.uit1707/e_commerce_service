﻿using Microsoft.Extensions.Configuration;

public class ConfigUtilities {
    public ConfigUtilities(string pathConfig) {
        Configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile(pathConfig, true, false)
            .Build();
    }

    public IConfiguration Configuration { get; }

    public string GetConnection(string key) {
        var result = Configuration.GetConnectionString(key);

        return result!;
    }

    public T GetConfig<T>(string key, T defaultValue) {
        try {
            var result = Configuration.GetSection(key).Get<T>();
            return result!;
        }
        catch (Exception) {
            if (defaultValue != null) return defaultValue;

            return default!;
        }
    }

    public T GetConfig<T>(string key) {
        try {
            var result = Configuration.GetSection(key).Get<T>();
            return result!;
        }
        catch (Exception) {
            throw new Exception(string.Format(
                "{0} is not found in the configuration file appSettings configuration, check profile configuration!",
                key));
        }
    }

    public IConfiguration GetSection(string key) {
        var result = Configuration.GetSection(key);

        return result;
    }
}