﻿using System.Text;

namespace e_commerce_service.common.Utilities {
    /// <summary>
    ///     Exception Utility Class
    /// </summary>
    public static class ExceptionUtilities {
        /// <summary>
        ///     Create Exception Message
        /// </summary>
        /// <param name="exception">Exception</param>
        /// <returns>Message</returns>
        public static string CreateExceptionMessage(this Exception exception, string requestId = "") {
            var errorMessage = new StringBuilder();

            if (string.IsNullOrEmpty(requestId)) {
                errorMessage.Append($"Request_Id: {requestId}").Append(Environment.NewLine);
            }

            while (exception != null) {
                errorMessage.Append("-> ").Append(exception.Message).Append(Environment.NewLine);
                errorMessage.Append(exception.StackTrace).Append(Environment.NewLine);

                exception = exception.InnerException!;
            }

            return errorMessage.ToString();
        }
    }
}
