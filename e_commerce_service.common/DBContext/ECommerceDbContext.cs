﻿using e_commerce_service.common.Models.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace e_commerce_service.common.DBContext {
    public interface IECommerceDbContext {
        Task<int> SaveChangesAsync();
        DatabaseFacade GetDatabase();
        int ExecuteSqlRaw(string sql);
        ChangeTracker ChangeTracker();

        DbSet<Product> Products { get; set; }
        DbSet<Variation> Variations { get; set; }
        DbSet<Category> Categories { get; set; }
        DbSet<Brand> Brands { get; set; }
        DbSet<BrandCategory> BrandCategories { get; set; }
        DbSet<Origin> Origins { get; set; }
        DbSet<OriginCategory> OriginCategories { get; set; }
        DbSet<Models.Entities.Attribute> Attributes { get; set; }
        DbSet<AttributeValue> AttributeValues { get; set; }
    }

    public class ECommerceDbContext : DbContext, IECommerceDbContext {
        #region Constructor
        public ECommerceDbContext() {

        }

        public ECommerceDbContext(DbContextOptions<ECommerceDbContext> options) : base(options) {
            Database.EnsureCreated();
        }
        #endregion

        #region Method
        protected override void OnModelCreating(ModelBuilder builder) {
            base.OnModelCreating(builder);
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true); //config để sử dụng DateTime mà ko cần phải là UTC (offset có thể là +7)
        }

        public async Task<int> SaveChangesAsync() {
            return await base.SaveChangesAsync();
        }

        public DatabaseFacade GetDatabase() {
            return base.Database;
        }

        public new ChangeTracker ChangeTracker() {
            return base.ChangeTracker;
        }

        public int ExecuteSqlRaw(string sql) {
            return base.Database.ExecuteSqlRaw(sql);
        }
        #endregion

        public DbSet<Product> Products { get; set; }
        public DbSet<Variation> Variations { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<BrandCategory> BrandCategories { get; set; }
        public DbSet<Origin> Origins { get; set; }
        public DbSet<OriginCategory> OriginCategories { get; set; }
        public DbSet<Models.Entities.Attribute> Attributes { get; set; }
        public DbSet<AttributeValue> AttributeValues { get; set; }
    }
}
