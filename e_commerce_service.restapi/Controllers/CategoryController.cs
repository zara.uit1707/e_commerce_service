using e_commerce_service.common.Models.Entities;
using e_commerce_service.common.Models.Responses;
using e_commerce_service.common.Services.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Portal.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class CategoryController : ControllerBase {
        private readonly ILogger<CategoryController> _logger;
        private readonly ICategoryServiceBLL _categoryServiceBLL;

        public CategoryController(
            ICategoryServiceBLL categoryServiceBLL,
            ILogger<CategoryController> logger) {
            _categoryServiceBLL = categoryServiceBLL;
            _logger = logger;
        }

        [HttpGet("{categoryId}")]
        public async Task<ActionResult<BaseResponse<Category>>> GetById(string categoryId) {
            var response = await _categoryServiceBLL.GetByIdAsync(categoryId);
            return response;
        }

        [HttpGet("Search")]
        public async Task<ActionResult<BaseResponse<List<Category>>>> Search(string content) {
            var response = await _categoryServiceBLL.SearchAsync(content);
            return response;
        }

        [HttpPost()]
        public async Task<ActionResult<BaseResponse<bool>>> Create(Category model) {
            var response = await _categoryServiceBLL.InsertAsync(model);
            return response;
        }

        [HttpPost("CreateMultiple")]
        public async Task<ActionResult<BaseResponse<bool>>> CreateMultiple(List<Category> models) {
            var response = await _categoryServiceBLL.InsertAsync(models);
            return response;
        }

        [HttpPut("{categoryId}")]
        public async Task<ActionResult<BaseResponse<bool>>> Update(string categoryId, Category model) {
            var response = await _categoryServiceBLL.UpdateAsync(categoryId, model);
            return response;
        }

        [HttpPut("Active/{categoryId}")]
        public async Task<ActionResult<BaseResponse<bool>>> ChangeActive(string categoryId) {
            var response = await _categoryServiceBLL.ChangeActiveAsync(categoryId, true);
            return response;
        }

        [HttpDelete("{categoryId}")]
        public async Task<ActionResult<BaseResponse<bool>>> Delete(string categoryId) {
            var response = await _categoryServiceBLL.ChangeActiveAsync(categoryId, false);
            return response;
        }
    }
}