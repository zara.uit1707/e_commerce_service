using e_commerce_service.common.Models.Entities;
using e_commerce_service.common.Models.Responses;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace Portal.Controllers {
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase {
        private readonly ILogger<ProductController> _logger;

        public ProductController(
            ILogger<ProductController> logger) {
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<BaseResponse<object>> Get() {
            var response = new BaseResponse<object>();
            response.Data = new {
                Id = ObjectId.GenerateNewId().ToString(),
                TimeNow = DateTime.UtcNow,
            };
            return response;
        }

        [HttpGet("product")]
        public ActionResult<BaseResponse<Product>> GetProduct() {
            var response = new BaseResponse<Product>();
            response.Data = new Product();
            return response;
        }
    }
}